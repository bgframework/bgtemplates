import sys
import click
import logging

from bgtemplates.templates.base import base
from bgtemplates.templates.pydata import pydata
from bgtemplates.templates.web import web
from . import __version__, logger_name

logger = logging.getLogger(logger_name)

CONTEXT_SETTINGS = dict(help_option_names=['-h', '--help'])


@click.group(context_settings=CONTEXT_SETTINGS)
@click.option('--debug', default=False, is_flag=True, help='Give more output (verbose)')
@click.version_option(version=__version__)
def cli(debug):
    """Create a project from one of our templates"""
    sh = logging.StreamHandler(sys.stdout)
    logger.setLevel(logging.DEBUG)
    if debug:
        fmt = logging.Formatter('%(asctime)s %(message)s', datefmt='%H:%M:%S')
        sh.setLevel(logging.DEBUG)
    else:
        fmt = logging.Formatter('... %(message)s', datefmt='%H:%M:%S')
        sh.setLevel(logging.INFO)
    sh.setFormatter(fmt)
    logger.addHandler(sh)
    logger.debug('Debug mode enabled')


cli.add_command(base)
cli.add_command(pydata)
cli.add_command(web)
