$(document).ready(function(){
    $.cookieBar({
        declineButton: false,
        message: 'This resource is intended for purely research purposes. We use cookies to track usage and preferences.'
    });
});
<!-- Tooltip -->
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})
