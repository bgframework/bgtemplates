// Template for generating dynamically the content
// of the user button in the nav-bar
// and the profile form

// FIXME: if you remove one of the option from tha navbar_user, you can also do it from here.


{% set user = getUser() %}
{% set user_profile = user.profile or none%}

$(document).ready(function() {

    {% if user is none %}
        anonymous();
    {% elif user_profile is none %}
        authenticate('{{user.id}}');
    {% else %}
        authenticate_with_profile('{{user_profile.name}}', '{{user_profile.email}}');

        {% if user_profile.institution is not none %}
            show_institution('{{user_profile.institution}}');
        {% endif %}

        newsletter({%if user_profile.newsletter%}true{%else%}false{%endif%});

        terms({%if user_profile.terms_accepted(app_name)%}true{%else%}false{%endif%});

        profile_submit_enable({%if user_profile.terms_accepted(app_name)%}true{%else%}false{%endif%});

    {% endif %}
});

{%if user_profile is not none %}
{%if not user_profile.terms_accepted(app_name)%}
    $(document).ready(function() {
        $('#profile_modal').modal({backdrop: 'static', keyboard: false});
        $('#profile_modal_close_icon').hide();
    });

    $(window).on('load', function() {
        $('#profile_modal').modal('show');
        $('#profile_form_newsletter').prop('checked', true);
    });
{% endif %}
{% endif %}