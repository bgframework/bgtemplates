#Configuration of the server where the actual application is running
[server]

host=localhost
port=8080

# Public URL, the one that is exposed (remember to add '/' at the end of the URL)
public_url = http://localhost:8080/

# Configuration for the different apps
[apps]

# Name of the applications
# The name is used to check whether the user has accepted the terms or not. It is also placed in
# different part of the web.
name = localhost_testing

# Each app must contain at least:
# *mount_point* where the application hangs with respect to the public URL
# Add any other parameter required by your application
# Additionally, each application accepts a subsection [[[server_conf]]] where you can directly pass
# configuration for cherrypy

    [[web]]
    mount_point = /
    websocket_domain=0.0.0.0:8080

    [[api]]
    mount_point = /api/v1

# Configuration of the scheduler that manages the jobs
[scheduler]

# Use 'local' to execute the jobs local at the server
# Use 'remote' to execute at a remote cluster
scheduler=local

# URL of the scheduler
scheduler_url=http://localhost:9090

# URL of the web server (same as the public URL but without the protocol and the final '/')
web_server=localhost:8080

# workspace for the jobs
workspace=/home/user/workspace

# Users that are administrators (from the jobs manager point of view)
#admin_users = example1@mail.org, example2@mail.org

# Maximum number of jobs a user can execute at the same time
concurrent_jobs = 4

# Maximum number of jobs a user can have saved
stored_jobs = 10

{% if project.anonymous -%}
# Maximum number of allowed anonymous jobs. Does not have impact if anonymous jobs are not enabled or is not set
anonymous_jobs = 5

# Seconds that any anonymous job is stored in our servers
anonymous_jobs_expire_time = 248832
{%- endif %}

# Security configuration
[security]

# File to log the users that login into our application
#registered_users = path/to/file/with/emails.txt

# Users that are administrators (from the server point of view)
#admin_users = example1@mail.org, example2@mail.org

[[auth0]]
# Auth0 parameters
AUTH0_CLIENT_ID=
AUTH0_DOMAIN=
AUTH0_CLIENT_SECRET=

#Configuration of the analysis
[analysis]
