Web project
===========

Web application that uses cherrypy.

The application has and additional API interface that uses REST.

To run the application you need to add the folder to the ``PYTHONPATH``:

.. code-block:: bash

   $ export PYTHONPATH="<path to the folder>"

   $ python <path to the folder>/bin/web.py


Structure
---------

::

   project/
   |
   |-- bin/
   |   |-- __init__.py
   |   |-- web.py
   |
   |-- conf/
   |   |-- system.conf.template
   |   |-- system.confspec
   |
   |-- project/
   |   |-- __init__.py
   |   |-- apiapp.py
   |   |-- webapp.py
   |
   |-- static/
   |   |...
   |
   |-- templates/
   |   |...


Additional features
-------------------

Conda environment
^^^^^^^^^^^^^^^^^

The build process will also suggest the creation of a conda environment.
If it is accepted, the project will also be installed in the new environment in
developer mode.


Version control
^^^^^^^^^^^^^^^

Finally, the option to start a git repository and to connect it to a remote
is also provided.
