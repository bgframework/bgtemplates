
import json
import os

import bglogs
import cherrypy
from bgweb import BGWeb
from bgweb.jobs import JobManagerError, JobNotFoundError, MaxStoredJobsError, MaxConcurrentJobsError, AuthorizationError
from bgweb.user import BGTokenUser as User
from bgweb.tools.authentication import AuthError, TokenNotFoundError, UserNotFoundError
from bgweb.tools.authentication.auth0 import Auth0Token
from bgweb.utils import errors_to_json, download, copy_file


@cherrypy.expose
class RESTAPIApp(BGWeb):

    def __init__(self, conf, jobs_manager):
        super().__init__(conf)

        self.server_conf['/']['request.dispatch'] = cherrypy.dispatch.MethodDispatcher()
        self.server_conf['/']['tools.response_headers.on'] = True
        self.server_conf['/']['tools.response_headers.headers'] = [('Content-Type', 'application/json')]
        self.server_conf['/']['error_page.default'] = errors_to_json

        self.jobs_manager = jobs_manager

    def setup(self, conf):
        self.app_name = conf['apps']['name']

        auth0_conf = conf['security']['auth0']
        domain = auth0_conf['AUTH0_DOMAIN']
        client = auth0_conf['AUTH0_CLIENT_ID']
        secret = auth0_conf['AUTH0_CLIENT_SECRET']
        self.auth_api = Auth0Token(domain, client, secret)

    def _check_rights(self, user, token):
        if user is None or token is None:
            raise cherrypy.HTTPError(400, 'Missing user email or token. Please sign up in the method website and request a token')

        try:
            valid = self.auth_api.check_token(user, token, self.app_name)
        except TokenNotFoundError:
            raise cherrypy.HTTPError(404, 'There is no token associated with this username. Please request one in the website')
        except UserNotFoundError:
            raise cherrypy.HTTPError(404, 'User not found. Please register in our website')
        except AuthError as e:
            raise cherrypy.HTTPError(500, 'Sorry, we could not verify your token. {}'.format(e.message))

        if not valid:
            bglogs.info('User %s using invalid token %s' % (user, token), _name='{{project.name}}.api')
            raise cherrypy.HTTPError(401, 'Unauthorized access. Token not valid')

    def __download(self, requester, id, file):
        try:
            directory = self.jobs_manager.get_directory(requester, id)  # check if the job exists and we have permissions
        except JobNotFoundError:
            raise cherrypy.HTTPError(404, 'Job not found')
        except AuthorizationError:
            raise cherrypy.HTTPError(403, 'Your are not authorized to access this job. Please request permissions to the owner.')
        except JobManagerError as e:
            bglogs.error('Error getting directory of job %s: %s' % (id, e.message), _name='{{project.name}}.api')
            raise cherrypy.HTTPError(500, e.message)

        download_file = os.path.join(directory, file)

        try:
            return download(download_file, '{}_{}'.format(id, file))
        except FileNotFoundError:
            raise cherrypy.HTTPError(404, 'File {} not found for job {}'.format(file, id))

    def GET(self, job_id=None, action=None, user=None, token=None):
        auth = cherrypy.request.headers.get('Authorization', None)
        if auth is not None:
            auth = auth.split(' ')
            try:
                token = auth[1]
                user = auth[0]
            except IndexError:
                pass
        self._check_rights(user, token)

        user = User(user)
        if job_id is None and action is None:
            jobs = self.jobs_manager.get_user_jobs(user)[user.id]
            return json.dumps(list(jobs.keys())).encode('utf-8')
        elif job_id is None:
            raise cherrypy.HTTPError(501, 'Method not found')
        else:
            try:
                job = self.jobs_manager.get_job(user, job_id)
            except JobNotFoundError:
                raise cherrypy.HTTPError(404, 'Job {} not found'.format(job_id))
            except AuthorizationError:
                raise cherrypy.HTTPError(403, 'You are not authorized to access this job')
            except JobManagerError as e:
                bglogs.error('Error getting job %s: %s' % (id, e.message), _name='{{project.name}}.api')
                raise cherrypy.HTTPError(500, e.message)
            if action is None:
                return json.dumps({'status': job.status,
                        'metadata': job.metadata
                        }).encode('utf-8')
            elif action == 'download':
                file = '' # FIXME add the file to be downloaded
                return self.__download(user, job_id, file)
            elif action == 'logs':
                return json.dumps({'status': job.status,
                        'logs': job.lines
                        }).encode('utf-8')
            else:
                bglogs.info('Action not found %s' % action, _name='{{project.name}}.api')
                raise cherrypy.HTTPError(501, 'Method not found')

    # FIXME fill the right parameters need for creating a job
    @cherrypy.tools.json_out()
    def POST(self, user=None, token=None, file1=None, file2=None, param1=None, param2=None):
        auth = cherrypy.request.headers.get('Authorization', None)
        if auth is not None:
            auth = auth.split(' ')
            try:
                token = auth[1]
                user = auth[0]
            except IndexError:
                pass
        self._check_rights(user, token)

        user = User(user)
        try:
            job_metadata, job_folder = self.jobs_manager.create_job(user)
        except MaxStoredJobsError:
            raise cherrypy.HTTPError(403,
                                     'You have reached the maximum number of jobs permitted. Please, delete one before submitting a new job.')
        except MaxConcurrentJobsError:
            raise cherrypy.HTTPError(403,
                                     'You have reached the maximum number of concurrent jobs permitted. Please, wait for the other jobs you have submitted.')

        if file1 is not None:
            # copy_file(file1, os.path.join(job_folder, file1.filename))
            copy_file(file1, '/home/user/Desktop/rest/{}'.format(file1.filename))

        if file2 is not None:
            # copy_file(file2, os.path.join(job_folder, file2.filename))
            copy_file(file2, '/home/user/Desktop/file1')

        command = '' #FIXME load command template from config
        job_id = self.jobs_manager.launch_job(command, job_metadata)
        raise NotImplementedError
        return job_id

    def DELETE(self, job_id, user=None, token=None):
        auth = cherrypy.request.headers.get('Authorization', None)
        if auth is not None:
            auth = auth.split(' ')
            try:
                token = auth[1]
                user = auth[0]
            except IndexError:
                pass
        self._check_rights(user, token)

        user = User(user)
        try:
            self.jobs_manager.remove(user, job_id)
        except JobNotFoundError:
            raise cherrypy.HTTPError(404, 'Job {} not found'.format(job_id))
        except AuthorizationError:
            raise cherrypy.HTTPError(403, 'You are not authorized to access this job')
        except JobManagerError as e:
            bglogs.error('Error removing job %s: %s' % (job_id, e.message), _name='{{project.name}}.api')
            raise cherrypy.HTTPError(500, e.message)

        return json.dumps('Deleted job {}'.format(job_id)).encode('utf-8')
