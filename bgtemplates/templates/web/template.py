import os
import click

from bgtemplates.bgproject import BGProject, create_project_structure
from bgtemplates.click_utils import click_check_project_name, ask_for_path, find_conda
from bgtemplates.conda import create_env_from_requerements_file
from bgtemplates.git import create_repo
from bgtemplates.tips import show_tips
from . import __version__




@click.command(short_help='Creates the basic structure for a scienfitic project with Python')
@click.option('--name', '-n', prompt='Name of the project', callback=click_check_project_name, help='Name of your project')
@click.version_option(version=__version__)
def web(name):
    """Create a Python project with basic scientific libraries"""

    project = BGProject()
    project.name = name

    project.path = ask_for_path(project.name)

    base_dir = os.path.dirname(os.path.abspath(__file__))

    if click.confirm('Would you like to allow anonymous jobs in your web?', default=True):
        project.anonymous = True
    else:
        project.anonymous = False

    conda = find_conda()
    if conda is not None:
        file = os.path.join(base_dir, 'requirements.txt')
        create_env_from_requerements_file(conda, name, file)

    unwanted_files = ['__init__.py', 'template.py', 'project_doc.rst', 'requirements.txt']
    unwanted_dirs = ['__pycache__', 'mypackage/__pycache__', 'bin/__pycache__']

    create_project_structure(base_dir, project, unwanted_files, unwanted_dirs)

    create_repo(project.path)

    show_tips()
