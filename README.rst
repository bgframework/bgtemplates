BgTemplates
===========

Create the structure of different projects.

Install
-------

Clone the repo and install with pip.

Command line
------------

Use the ``bgtemplates`` command to use this library::

   $ bgtemplates --help

Documentation
-------------

Build the documentation using Sphinx::

   $ conda install --file optional-requirements.txt
   $ cd docs
   $ make html

.. note::

   If you prefer to use pip to install the ``optional-requirements.txt``::

      $ pip install -r optional-requirements.txt
