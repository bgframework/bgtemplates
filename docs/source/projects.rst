########
Projects
########

The different projects that are supported:

.. include:: ../../bgtemplates/templates/base/project_doc.rst

----

.. include:: ../../bgtemplates/templates/pydata/project_doc.rst

----

.. include:: ../../bgtemplates/templates/web/project_doc.rst
