bgtemplates.templates.pydata package
====================================

Subpackages
-----------

.. toctree::

    bgtemplates.templates.pydata.mypackage

Submodules
----------

bgtemplates.templates.pydata.template module
--------------------------------------------

.. automodule:: bgtemplates.templates.pydata.template
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: bgtemplates.templates.pydata
    :members:
    :undoc-members:
    :show-inheritance:
