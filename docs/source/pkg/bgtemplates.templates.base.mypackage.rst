bgtemplates.templates.base.mypackage package
============================================

Submodules
----------

bgtemplates.templates.base.mypackage.main module
------------------------------------------------

.. automodule:: bgtemplates.templates.base.mypackage.main
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: bgtemplates.templates.base.mypackage
    :members:
    :undoc-members:
    :show-inheritance:
