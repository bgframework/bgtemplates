bgtemplates.templates.web package
=================================

Subpackages
-----------

.. toctree::

    bgtemplates.templates.web.mypackage

Submodules
----------

bgtemplates.templates.web.template module
-----------------------------------------

.. automodule:: bgtemplates.templates.web.template
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: bgtemplates.templates.web
    :members:
    :undoc-members:
    :show-inheritance:
