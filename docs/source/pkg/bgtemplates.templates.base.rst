bgtemplates.templates.base package
==================================

Subpackages
-----------

.. toctree::

    bgtemplates.templates.base.mypackage

Submodules
----------

bgtemplates.templates.base.template module
------------------------------------------

.. automodule:: bgtemplates.templates.base.template
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: bgtemplates.templates.base
    :members:
    :undoc-members:
    :show-inheritance:
