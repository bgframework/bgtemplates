bgtemplates package
===================

Subpackages
-----------

.. toctree::

    bgtemplates.templates

Submodules
----------

bgtemplates.bgproject module
----------------------------

.. automodule:: bgtemplates.bgproject
    :members:
    :undoc-members:
    :show-inheritance:

bgtemplates.bgtemplates module
------------------------------

.. automodule:: bgtemplates.bgtemplates
    :members:
    :undoc-members:
    :show-inheritance:

bgtemplates.click_utils module
------------------------------

.. automodule:: bgtemplates.click_utils
    :members:
    :undoc-members:
    :show-inheritance:

bgtemplates.conda module
------------------------

.. automodule:: bgtemplates.conda
    :members:
    :undoc-members:
    :show-inheritance:

bgtemplates.git module
----------------------

.. automodule:: bgtemplates.git
    :members:
    :undoc-members:
    :show-inheritance:

bgtemplates.tips module
-----------------------

.. automodule:: bgtemplates.tips
    :members:
    :undoc-members:
    :show-inheritance:

bgtemplates.utils module
------------------------

.. automodule:: bgtemplates.utils
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: bgtemplates
    :members:
    :undoc-members:
    :show-inheritance:
