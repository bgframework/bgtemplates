bgtemplates.templates package
=============================

Subpackages
-----------

.. toctree::

    bgtemplates.templates.base
    bgtemplates.templates.pydata
    bgtemplates.templates.web

Module contents
---------------

.. automodule:: bgtemplates.templates
    :members:
    :undoc-members:
    :show-inheritance:
