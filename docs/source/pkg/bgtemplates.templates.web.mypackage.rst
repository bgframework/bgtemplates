bgtemplates.templates.web.mypackage package
===========================================

Submodules
----------

bgtemplates.templates.web.mypackage.apiapp module
-------------------------------------------------

.. automodule:: bgtemplates.templates.web.mypackage.apiapp
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: bgtemplates.templates.web.mypackage
    :members:
    :undoc-members:
    :show-inheritance:
