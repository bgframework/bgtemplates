BGTemplates
===========

BGTemplates is a Python project intended to ease the creation of
Python projects as well as encourage good practices.

Best practices encouraged:

#. Using conda environments
#. Using git