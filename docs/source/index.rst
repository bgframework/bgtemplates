.. bgtemplates documentation master file, created by
   sphinx-quickstart on Fri Feb  3 12:04:51 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bgtemplates's documentation!
=======================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   intro
   projects



.. toctree::
   :hidden:
   :caption: Code documentation

   pkg/modules.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
